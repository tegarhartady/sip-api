import Vue from 'vue'
import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from '../layouts/error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'
import { createStore } from './store.js'

/* Plugins */

import nuxt_plugin_workbox_0f61eb38 from 'nuxt_plugin_workbox_0f61eb38' // Source: ./workbox.js (mode: 'client')
import nuxt_plugin_nuxticons_674da200 from 'nuxt_plugin_nuxticons_674da200' // Source: ./nuxt-icons.js (mode: 'all')
import nuxt_plugin_pluginrouting_4e6c0704 from 'nuxt_plugin_pluginrouting_4e6c0704' // Source: ./nuxt-i18n/plugin.routing.js (mode: 'all')
import nuxt_plugin_pluginmain_0b3e6fb1 from 'nuxt_plugin_pluginmain_0b3e6fb1' // Source: ./nuxt-i18n/plugin.main.js (mode: 'all')
import nuxt_plugin_googleanalytics_82ef9844 from 'nuxt_plugin_googleanalytics_82ef9844' // Source: ./google-analytics.js (mode: 'client')
import nuxt_plugin_toast_026393f7 from 'nuxt_plugin_toast_026393f7' // Source: ./toast.js (mode: 'client')
import nuxt_plugin_axios_36334748 from 'nuxt_plugin_axios_36334748' // Source: ./axios.js (mode: 'all')
import nuxt_plugin_gtm_09257fc4 from 'nuxt_plugin_gtm_09257fc4' // Source: ./gtm.js (mode: 'all')
import nuxt_plugin_vuexpersist_13f465a2 from 'nuxt_plugin_vuexpersist_13f465a2' // Source: ../plugins/vuex-persist (mode: 'all')
import nuxt_plugin_vtooltip_74057214 from 'nuxt_plugin_vtooltip_74057214' // Source: ../plugins/v-tooltip (mode: 'all')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)

// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render (h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true

      console.warn('<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead')
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>
Vue.component(Nuxt.name, Nuxt)

Vue.use(Meta, {"keyName":"head","attribute":"data-n-head","ssrAttribute":"data-n-head-ssr","tagIDKeyName":"hid"})

const defaultTransition = {"name":"page","mode":"out-in","appear":true,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

async function createApp (ssrContext) {
  const router = await createRouter(ssrContext)

  const store = createStore(ssrContext)
  // Add this.$router into store actions/mutations
  store.$router = router

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    head: {"title":"Postwoman • A free, fast and beautiful API request builder","meta":[{"name":"keywords","content":"postwoman, postwoman chrome, postwoman online, postwoman for mac, postwoman app, postwoman for windows, postwoman google chrome, postwoman chrome app, get postwoman, postwoman web, postwoman android, postwoman app for chrome, postwoman mobile app, postwoman web app, api, request, testing, tool, rest, websocket, sse, graphql, socketio"},{"name":"X-UA-Compatible","content":"IE=edge, chrome=1"},{"itemprop":"name","content":"Postwoman • A free, fast and beautiful API request builder"},{"itemprop":"description","content":"Web alternative to Postman - Helps you create requests faster, saving precious time on development."},{"itemprop":"image","content":"https:\u002F\u002Fpostwoman.io\u002Flogo.jpg"},{"property":"og:image","content":"https:\u002F\u002Fpostwoman.io\u002Flogo.jpg"},{"name":"application-name","content":"Postwoman"},{"name":"msapplication-TileImage","content":"\u002Ficons\u002Ficon-144x144.png"},{"name":"msapplication-TileColor","content":"#202124"},{"name":"msapplication-tap-highlight","content":"no"},{"hid":"charset","charset":"utf-8"},{"hid":"viewport","name":"viewport","content":"width=device-width, initial-scale=1"},{"hid":"mobile-web-app-capable","name":"mobile-web-app-capable","content":"yes"},{"hid":"apple-mobile-web-app-title","name":"apple-mobile-web-app-title","content":"Postwoman"},{"hid":"author","name":"author","content":"liyasthomas"},{"hid":"description","name":"description","content":"A free, fast and beautiful API request builder"},{"hid":"theme-color","name":"theme-color","content":"#202124"},{"hid":"og:type","name":"og:type","property":"og:type","content":"website"},{"hid":"og:title","name":"og:title","property":"og:title","content":"Postwoman"},{"hid":"og:site_name","name":"og:site_name","property":"og:site_name","content":"Postwoman"},{"hid":"og:description","name":"og:description","property":"og:description","content":"A free, fast and beautiful API request builder"},{"hid":"og:url","name":"og:url","property":"og:url","content":"https:\u002F\u002Fpostwoman.io"},{"hid":"twitter:card","name":"twitter:card","property":"twitter:card","content":"summary_large_image"},{"hid":"twitter:site","name":"twitter:site","property":"twitter:site","content":"@liyasthomas"},{"hid":"twitter:creator","name":"twitter:creator","property":"twitter:creator","content":"@liyasthomas"}],"link":[{"rel":"icon","type":"image\u002Fx-icon","href":"\u002Ffavicon.ico"},{"rel":"apple-touch-icon","href":"\u002Ficons\u002Ficon-48x48.png"},{"rel":"apple-touch-icon","sizes":"72x72","href":"\u002Ficons\u002Ficon-72x72.png"},{"rel":"apple-touch-icon","sizes":"96x96","href":"\u002Ficons\u002Ficon-96x96.png"},{"rel":"apple-touch-icon","sizes":"144x144","href":"\u002Ficons\u002Ficon-144x144.png"},{"rel":"apple-touch-icon","sizes":"192x192","href":"\u002Ficons\u002Ficon-192x192.png"},{"rel":"manifest","href":"\u002F_nuxt\u002Fmanifest.6641b126.json"}],"style":[],"script":[{"hid":"gtm-script","innerHTML":"if(!window._gtm_init){window._gtm_init=1;(function (w,n,d,m,e,p){w[d]=(w[d]==1||n[d]=='yes'||n[d]==1||n[m]==1||(w[e]&&w[e].p&&e[e][p]()))?1:0})(window,'navigator','doNotTrack','msDoNotTrack','external','msTrackingProtectionEnabled');(function(w,d,s,l,x,y){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});w[x]={};w._gtm_inject=function(i){if(w.doNotTrack||w[x][i])return;w[x][i]=1;var f=d.getElementsByTagName(s)[0],j=d.createElement(s);j.async=true;j.src='https:\u002F\u002Fwww.googletagmanager.com\u002Fgtm.js?id='+i;f.parentNode.insertBefore(j,f);};w[y]('GTM-MXWD8NQ')})(window,document,'script','dataLayer','_gtm_ids','_gtm_inject')}"}],"noscript":[{"hid":"gtm-noscript","pbody":true,"innerHTML":"\u003Ciframe src=\"https:\u002F\u002Fwww.googletagmanager.com\u002Fns.html?id=GTM-MXWD8NQ&\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\" title=\"gtm\"\u003E\u003C\u002Fiframe\u003E"}],"__dangerouslyDisableSanitizersByTagID":{"gtm-script":["innerHTML"],"gtm-noscript":["innerHTML"]},"htmlAttrs":{"lang":"en"}},

    store,
    router,
    nuxt: {
      defaultTransition,
      transitions: [defaultTransition],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [transitions]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },

      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        let nuxt = app.nuxt // to work with @vue/composition-api, see https://github.com/nuxt/nuxt.js/issues/6517#issuecomment-573280207
        if (this) {
          nuxt = this.nuxt || this.$options.nuxt
        }
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) {
          ssrContext.nuxt.error = err
        }
        return err
      }
    },
    ...App
  }

  // Make app available into store via this.app
  store.app = app

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base, router.options.mode)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    store,
    route,
    next,
    error: app.nuxt.error.bind(app),
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    ssrContext
  })

  const inject = function (key, value) {
    if (!key) {
      throw new Error('inject(key, value) has no key provided')
    }
    if (value === undefined) {
      throw new Error(`inject('${key}', value) has no value provided`)
    }

    key = '$' + key
    // Add into app
    app[key] = value

    // Add into store
    store[key] = app[key]

    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) {
      return
    }
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Object.prototype.hasOwnProperty.call(Vue, key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  if (process.client) {
    // Replace store state before plugins execution
    if (window.__NUXT__ && window.__NUXT__.state) {
      store.replaceState(window.__NUXT__.state)
    }
  }

  // Plugin execution

  if (process.client && typeof nuxt_plugin_workbox_0f61eb38 === 'function') {
    await nuxt_plugin_workbox_0f61eb38(app.context, inject)
  }

  if (typeof nuxt_plugin_nuxticons_674da200 === 'function') {
    await nuxt_plugin_nuxticons_674da200(app.context, inject)
  }

  if (typeof nuxt_plugin_pluginrouting_4e6c0704 === 'function') {
    await nuxt_plugin_pluginrouting_4e6c0704(app.context, inject)
  }

  if (typeof nuxt_plugin_pluginmain_0b3e6fb1 === 'function') {
    await nuxt_plugin_pluginmain_0b3e6fb1(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_googleanalytics_82ef9844 === 'function') {
    await nuxt_plugin_googleanalytics_82ef9844(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_toast_026393f7 === 'function') {
    await nuxt_plugin_toast_026393f7(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_36334748 === 'function') {
    await nuxt_plugin_axios_36334748(app.context, inject)
  }

  if (typeof nuxt_plugin_gtm_09257fc4 === 'function') {
    await nuxt_plugin_gtm_09257fc4(app.context, inject)
  }

  if (typeof nuxt_plugin_vuexpersist_13f465a2 === 'function') {
    await nuxt_plugin_vuexpersist_13f465a2(app.context, inject)
  }

  if (typeof nuxt_plugin_vtooltip_74057214 === 'function') {
    await nuxt_plugin_vtooltip_74057214(app.context, inject)
  }

  // If server-side, wait for async component to be resolved first
  if (process.server && ssrContext && ssrContext.url) {
    await new Promise((resolve, reject) => {
      router.push(ssrContext.url, resolve, () => {
        // navigated to a different route in router guard
        const unregister = router.afterEach(async (to, from, next) => {
          ssrContext.url = to.fullPath
          app.context.route = await getRouteData(to)
          app.context.params = to.params || {}
          app.context.query = to.query || {}
          unregister()
          resolve()
        })
      })
    })
  }

  return {
    store,
    app,
    router
  }
}

export { createApp, NuxtError }
