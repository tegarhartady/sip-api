export const vueI18n = {"fallbackLocale":"en"}
export const vueI18nLoader = false
export const locales = [{"code":"en","name":"English","iso":"en-US","file":"en-US.json"},{"code":"es","name":"Español","iso":"es-ES","file":"es-ES.json"},{"code":"fr","name":"Français","iso":"fr-FR","file":"fr-FR.json"},{"code":"fa","name":"Farsi","iso":"fa-IR","file":"fa-IR.json"},{"code":"pt","name":"Português Brasileiro","iso":"pt-BR","file":"pt-BR.json"},{"code":"cn","name":"简体中文","iso":"zh-CN","file":"zh-CN.json"},{"code":"id","name":"Bahasa Indonesia","iso":"id-ID","file":"id-ID.json"},{"code":"tr","name":"Türkçe","iso":"tr-TR","file":"tr-TR.json"},{"code":"de","name":"Deutsch","iso":"de-DE","file":"de-DE.json"},{"code":"ja","name":"日本語","iso":"ja-JP","file":"ja-JP.json"},{"code":"ko","name":"한국어","iso":"ko-KR","file":"ko-KR.json"}]
export const defaultLocale = 'en'
export const routesNameSeparator = '___'
export const defaultLocaleRouteNameSuffix = 'default'
export const strategy = 'prefix_except_default'
export const lazy = true
export const langDir = 'lang/'
export const rootRedirect = null
export const detectBrowserLanguage = {"useCookie":true,"cookieDomain":null,"cookieKey":"i18n_redirected","alwaysRedirect":false,"fallbackLocale":null}
export const differentDomains = false
export const seo = false
export const baseUrl = ''
export const vuex = {"moduleName":"i18n","syncLocale":false,"syncMessages":false,"syncRouteParams":true}
export const parsePages = true
export const pages = {}
export const encodePaths = true
export const beforeLanguageSwitch = () => null
export const onLanguageSwitched = () => null
export const MODULE_NAME = 'nuxt-i18n'
export const LOCALE_CODE_KEY = 'code'
export const LOCALE_ISO_KEY = 'iso'
export const LOCALE_DOMAIN_KEY = 'domain'
export const LOCALE_FILE_KEY = 'file'
export const STRATEGIES = {"PREFIX":"prefix","PREFIX_EXCEPT_DEFAULT":"prefix_except_default","PREFIX_AND_DEFAULT":"prefix_and_default","NO_PREFIX":"no_prefix"}
export const COMPONENT_OPTIONS_KEY = 'nuxtI18n'
export const localeCodes = ["en","es","fr","fa","pt","cn","id","tr","de","ja","ko"]
