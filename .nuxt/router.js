import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _09fbc5fc = () => interopDefault(import('../pages/doc.vue' /* webpackChunkName: "pages/doc" */))
const _6ae383a6 = () => interopDefault(import('../pages/graphql.vue' /* webpackChunkName: "pages/graphql" */))
const _372443d7 = () => interopDefault(import('../pages/realtime.vue' /* webpackChunkName: "pages/realtime" */))
const _ea7192e2 = () => interopDefault(import('../pages/settings.vue' /* webpackChunkName: "pages/settings" */))
const _eb579cd4 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/doc",
    component: _09fbc5fc,
    name: "doc___en"
  }, {
    path: "/graphql",
    component: _6ae383a6,
    name: "graphql___en"
  }, {
    path: "/realtime",
    component: _372443d7,
    name: "realtime___en"
  }, {
    path: "/settings",
    component: _ea7192e2,
    name: "settings___en"
  }, {
    path: "/cn/",
    component: _eb579cd4,
    name: "index___cn"
  }, {
    path: "/cn/doc",
    component: _09fbc5fc,
    name: "doc___cn"
  }, {
    path: "/cn/graphql",
    component: _6ae383a6,
    name: "graphql___cn"
  }, {
    path: "/cn/realtime",
    component: _372443d7,
    name: "realtime___cn"
  }, {
    path: "/cn/settings",
    component: _ea7192e2,
    name: "settings___cn"
  }, {
    path: "/de/",
    component: _eb579cd4,
    name: "index___de"
  }, {
    path: "/de/doc",
    component: _09fbc5fc,
    name: "doc___de"
  }, {
    path: "/de/graphql",
    component: _6ae383a6,
    name: "graphql___de"
  }, {
    path: "/de/realtime",
    component: _372443d7,
    name: "realtime___de"
  }, {
    path: "/de/settings",
    component: _ea7192e2,
    name: "settings___de"
  }, {
    path: "/es/",
    component: _eb579cd4,
    name: "index___es"
  }, {
    path: "/es/doc",
    component: _09fbc5fc,
    name: "doc___es"
  }, {
    path: "/es/graphql",
    component: _6ae383a6,
    name: "graphql___es"
  }, {
    path: "/es/realtime",
    component: _372443d7,
    name: "realtime___es"
  }, {
    path: "/es/settings",
    component: _ea7192e2,
    name: "settings___es"
  }, {
    path: "/fa/",
    component: _eb579cd4,
    name: "index___fa"
  }, {
    path: "/fa/doc",
    component: _09fbc5fc,
    name: "doc___fa"
  }, {
    path: "/fa/graphql",
    component: _6ae383a6,
    name: "graphql___fa"
  }, {
    path: "/fa/realtime",
    component: _372443d7,
    name: "realtime___fa"
  }, {
    path: "/fa/settings",
    component: _ea7192e2,
    name: "settings___fa"
  }, {
    path: "/fr/",
    component: _eb579cd4,
    name: "index___fr"
  }, {
    path: "/fr/doc",
    component: _09fbc5fc,
    name: "doc___fr"
  }, {
    path: "/fr/graphql",
    component: _6ae383a6,
    name: "graphql___fr"
  }, {
    path: "/fr/realtime",
    component: _372443d7,
    name: "realtime___fr"
  }, {
    path: "/fr/settings",
    component: _ea7192e2,
    name: "settings___fr"
  }, {
    path: "/id/",
    component: _eb579cd4,
    name: "index___id"
  }, {
    path: "/id/doc",
    component: _09fbc5fc,
    name: "doc___id"
  }, {
    path: "/id/graphql",
    component: _6ae383a6,
    name: "graphql___id"
  }, {
    path: "/id/realtime",
    component: _372443d7,
    name: "realtime___id"
  }, {
    path: "/id/settings",
    component: _ea7192e2,
    name: "settings___id"
  }, {
    path: "/ja/",
    component: _eb579cd4,
    name: "index___ja"
  }, {
    path: "/ja/doc",
    component: _09fbc5fc,
    name: "doc___ja"
  }, {
    path: "/ja/graphql",
    component: _6ae383a6,
    name: "graphql___ja"
  }, {
    path: "/ja/realtime",
    component: _372443d7,
    name: "realtime___ja"
  }, {
    path: "/ja/settings",
    component: _ea7192e2,
    name: "settings___ja"
  }, {
    path: "/ko/",
    component: _eb579cd4,
    name: "index___ko"
  }, {
    path: "/ko/doc",
    component: _09fbc5fc,
    name: "doc___ko"
  }, {
    path: "/ko/graphql",
    component: _6ae383a6,
    name: "graphql___ko"
  }, {
    path: "/ko/realtime",
    component: _372443d7,
    name: "realtime___ko"
  }, {
    path: "/ko/settings",
    component: _ea7192e2,
    name: "settings___ko"
  }, {
    path: "/pt/",
    component: _eb579cd4,
    name: "index___pt"
  }, {
    path: "/pt/doc",
    component: _09fbc5fc,
    name: "doc___pt"
  }, {
    path: "/pt/graphql",
    component: _6ae383a6,
    name: "graphql___pt"
  }, {
    path: "/pt/realtime",
    component: _372443d7,
    name: "realtime___pt"
  }, {
    path: "/pt/settings",
    component: _ea7192e2,
    name: "settings___pt"
  }, {
    path: "/tr/",
    component: _eb579cd4,
    name: "index___tr"
  }, {
    path: "/tr/doc",
    component: _09fbc5fc,
    name: "doc___tr"
  }, {
    path: "/tr/graphql",
    component: _6ae383a6,
    name: "graphql___tr"
  }, {
    path: "/tr/realtime",
    component: _372443d7,
    name: "realtime___tr"
  }, {
    path: "/tr/settings",
    component: _ea7192e2,
    name: "settings___tr"
  }, {
    path: "/",
    component: _eb579cd4,
    name: "index___en"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
